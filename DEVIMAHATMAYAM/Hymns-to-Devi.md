<span style="color:; font-family:Menlo, Consolas, DejaVu Sans Mono, monospace; font-size: 12pt ;">

I worship in my heart
The Devi whose body is most as nectar.
Beauteous as the splendour of lightning.
Who, going from Her abode to that of Shiva,
Opens the lotuses on the beautiful way. of the Susumna

---

I remember again and again
The dark primeval Devi
Swayed with passion,
Her beauteous face heated and moist
With sweat of amorous play.
Bearing a necklace of Ganja berries.
And clad with leaves.
O Spouse of Srikantha,
I place on my head Thy blue lotus feet,
Which are followed by the Vedas,
As swans are lured by the tinkling sound of an anklet.

---

O Bhavani! I worship thy body from ankle to knee,
Upon which the bull bannered one
Gazes thereon with two eyes,
Has yet made for himself a third.

---
