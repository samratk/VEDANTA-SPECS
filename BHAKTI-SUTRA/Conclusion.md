<span style="color:; font-family:Menlo, Consolas, DejaVu Sans Mono, monospace; font-size: 12pt ;">

<h4> || उपसन्हार: || </h4>

---
>  82. गुणमाहात्म्यासक्ति-रूपासक्ति-पूजासक्ति-स्मरणासक्ति-दास्यासक्ति-सख्यासक्ति-
      वात्सल्यसक्ति-कान्तासक्ति-आत्मनिवेदनासक्ति-तन्मयतासक्ति-परमविरहासक्ति-रूपा
      एकधा अपि एकादशधा भवति ।

sutra 2 and 7 - roopa had come.
एकधा अपि एकादशधा भवति - Although it is just one Bhakti - parama prem roopa, it appears as 11 forms. It is described in 11 ways.

Asakti - addiction - sticking to it, attachment. Asakti in life is condemned. Vishaya tyaga, kaam tyaga...here Asakti is for that this form of Bhakti that we have developed.

1. Guna Mahatma asakti - All the best qualities in man. Greatness. Swarup of God - Transcend and immanent. Gopi described it too. The best description of God -
   - Transcendent-Immanent,
   - Existence-Consciousness-Bliss
   - Absolute-One without a second.
2. Roopa asakti - Addiction to the form of God. Ramaprasad songs.
3. Puja sakti - Puaji di shu anuraga came earlier. One who is not ready to give up worship.
4. Smarna Asakti - remember the name of the God.
5. Dasya sakti - Give me some work o Lord. I cannot keep quiet without doing something for God.
6. Vatsalya Sakti - I do not want anything from God. I will give my everything to God. God is someone to be loved unselfishly.
7. Atma nivedan - Self being dedicated to God. Completely dedicatd to God. They become zero. Emptied my everything to God.
8. Tanmaya sakti - Lost in God.Filled with God. From hair to nails. Fire is in wood - everywhere.
Uddava -
9. Param viraha asakti - always weeping for God.

>83. इत्येवं वदन्ति जनजल्पनिर्भयाः एकमताः
    कुमार-व्यास-शुक-शाण्डिल्य-गर्ग-विष्णु-
    कौण्डिण्य-शेषोद्धवारुणि-बलि-हनुमद्-विभीषणादयो भक्त्याचार्याः ।

From sutra 1 - 82 - whatever has been told is Iti. Jana Jalpana - useless talks of the common people. The Bhaktas is nirbhaya of these jana jalpanas. There is no difference in all the teachings of these Great People. Ordinary people talk a lot.
1. Kumar - Sanat Kumar - Narada's Guru - 7th chapter of Chandogya
2. Vyasa - It is a upadhi name. Many people have this name Vyasa. Here he refers to Krishna Dwaipanya Vyasa.
3. Shuka Deva - son of Vyasa
4. Shandilya - Company of Narada - Chandogya 3rd chapter 14th section. Shandliya's teaching on Bhakti.
5. Garga - Guru of Krishna in Vrindavan.
6. Vishnu -
7. Koundilya - Shandilya's son
8. Sheshenag - Laxman and Balaram.

>84. य इदं नारदप्रोक्तं शिवानुशासनं विश्वसिति श्रद्धते स भक्तिमान्
      भवति सः प्रेष्टं लभते सः प्रेष्टं लभते ।

Narada has spoken the science taught from other masters. Shiva - auspicious. Vishva Siti -
Shraddha - i do not see God. Masters have seen. I will follow their path and will see God.
Preshtham - dearest thing in life.

---
प्रथमोऽध्यायः  - भक्तिस्वरूपम् । सूत्र 1-33
द्वितीयोऽध्यायः - भक्ति साधना । सूत्र 34-55
तृतीयोऽध्यायः - गौणी भक्ति: । सूत्र 56-81
उपसन्हार    -  सूत्र 82-84
