  <h5> द्वितियोऽध्यायः - परिच्छेद 2 - माया सन्तरणम् </h5>
The changes that come in your mind due to Bhakti? It helps you to go beyond Maya. Maya Santaranam - When Naradiya Bhakti has come to you, there will be changes that will come into your life that will take you beyond Maya.

---

  46. कस्तरति कस्तरति मायाम् यः सङ्गं त्यजति यो महानुभावं सेवते निर्ममो भवति ।
  `Sevete - who takes resort to Mahanubhavas. Mahanubhavas - whose heart is great. who has reached the jeevan mukta.`
  >`At what point of time in life, by what changes in my life happens that I cross Maya? 1st two are representatives of all the means of sadhnaa. From visaya tyaga he chooses sangha tyaga..because unless you give up the objects of enjoyment you can never give up attachment. Sanga tyaga includes Visya tyaga. This leads to Nirmamo - no mine! World is not real. So there is nothing mine. Also everything belongs to God. So, three is no object that is mine.`
  >`1. How can this be mine, if everything belongs to God. 2. World is not real. 3. If at all something mine, it is God`
  47. यो विविक्तस्थानं सेवते यो लोकबन्धमुन्मूलयति निस्त्रैगुण्यो भवति योगक्षेमं त्यजति ।
  >`Vivikta - nirjan - chosen. The word comes from Viveka. A secluded holy place. Sutra 14 is connected with this. Everything in the society binds him. Society is an obstruction to go to God. Uproot the Loka bandhham. `
  `Lokobandham - restrictions of the society uproots and throws them away. He does not worry what he will get and eat and live tomorrow.`

  48. यः कर्मफलं त्यजति कर्माणि संन्यस्स्यति ततो निर्द्वन्द्वो भवति ।
  >`The Karma Fala - enjoyment of meditation and bhakti. The attitude to cash Bhakti. relinquish. Karma - anything done with the sense of I am the doer. Then the dualism of life does not effect him. if there respect, there is dis-resepct. if there is love, there is hatred. But when karmaphala is relinquished nothing matters. Karma Sanyasa - Give up the doership. It is God who is doing. I the doer breaks. I the lover, I the experiencer remains.`
  >`Example - Shashi Maharaj - no food - go to beach and made food of sand. Pleasant kindness and unpleasant kindness`
  49. यो वेदानपि संन्यस्यति केवलमविच्छिन्नानुरागं लभते ।
  >`Veda - shashtras. The only scriptures says that let the man of realization goes beyond me. Your life becomes a shashtra. Mind itself becomes a Guru. After realization Shashtra is not needed. Verse 12 connected. Keval love remains. You too do not remain. Anurag - love for God. Unbroken love only remains.`
  50. स तरति स तरति स लोकांस्तारयति ।
  >` He crosses the Maya and definitedly crosses who follows all these steps. He becomes Mahapurusha. He starts helping people to cross. Maya is a deception. If we are in the flow of Bhakti, the changes will happen in us. There is no choice.`

---
