<h4> || द्वितियोऽध्यायः - भक्ति साधना|| </h4>
<h5> द्वितियोऽध्यायः - परिच्छेद 1 - भक्ति साधनानि </h5>
<h6> The means to achieve (not practice) this Bhakti </h6>

---
  34. तस्याः साधनानि गायन्त्याचार्याः ।
  ``
  35. तत्तु विषयत्यागात् सङ्गत्यागात् च ।

  >`Tu - not our Bhakti. Narada is telling us not to do something . Dhyan bhajan. Meditation with devotion. Vishaya tyaga - leave all the [objects] from which we feel sense of enjoyment. And also we need to give up the [attachment] to those objects. Bhakti which is very natural is hidden by obstructions. These obstructions are as follows - family, friends, minds, dress, eat, food. Don't keep anything that causes attachment. If you do something once, and then you want to repeat it, it is attachment. `

  >`Do not do anything to enjoy. Now you study and then enjoy afterwards, people say. Pig rules in enjoyment. It does not study. Why should study connect with enjoyment? Study is done for improvement of Buddhi. Chaitanya Mahaprabhu - elder brother. after completion of study he went to forest. To understand good and bad, dirty and not dirty, we study!`

  >`Vishaya - objects of enjoyment. Give up the objects of enjoyment. It is not a joke. It is not a childsplay. Along with Vishaya, give up the attachment to them too - Sanghat cha. `

  >`If a man has no desires, religion will be just - O Lord you are so wonderful!`

  36. अव्यावृत्तभजनात् ।

  >`Only after giving up sense objects of enjoyment and attachment nto them, Do not Circumscribe (Vyavri) to your acts of devotion (Bhajan) - reading of scriptures, puja, meditation, japa, etc. Do anything out of devotion. Do not circumscribe them. Make every action devotional action. I study to know God. I study because God wants so - secular as well as spiritual `

  37. लोकेऽपि भगवद्गुणश्रवणकीर्तनात् ।
  >`Make the secular actions as acts of devotion. Keep a part of the mind in God always. Listen to or sing to yourself. This is the means of achieving Nardiya Bhakti. Otherwise mind will go astray.`

  38. मुख्यतस्तु महत्कृपयैव भगवत्कृपालेशाद् वा ।
  >`But mainly one gets Bhakti by the Bhakti of Guru. Only when Spring comes, you get flowers. Even after your hard work, only when a great man comes you get Bhakti`
  >`The main means - By the grace of Mahapurushas. By the grace of great men who have realized God. The main means of getting Bhakti of Narada is to be in the flow of grace of the Mahapurushas. But this only happens when the above actions are taken. Bhagavad Kripa Lesha - this mahat kripa - mahapurusha kripa - is a kripa lesha (a small particle) of God.  `
  >`Dont seek exception. Seek general rule`

  39. महत्सङ्गस्तु दुर्लभोऽगम्योऽमोघश्च ।
  >`How to get this Mahat Sangha? How to avoid the fake Gurus. Tu - But - Association with Mahat is durlabh - rare. Durlabh. But if you get  him His grace if Amogha - unfallible. Understanding Guru is difficult. But there are methods. - Shishya - purity of chitta, genuine desire to God, patience. A seeking sinner gets the seeking savior. Sakti Paath - makes you weak. You need to do sadhana. Gurus who do not make devotees do sadhana, shun them.`

  >`This seems to be not in our hands. To get the connection with Mahapurushas. But when you are pure love of God, and sincerely seek, you will get one. Should we wait for it? Where will we get it?`

  40. लभ्यतेऽपि तत्कृपयैव ।
  >`You will get that true Guru by God's Grace. You will get it. If you are seeking God, God will see to it that you will get the Mahapurusha, if you are sincerely seek God. A seeking sinner sees the seeking savior.`
  `If you get a Mahapurusha, please remember and remind yourself that it is God's grace that you have got him. You never get Guru based on your own greatness or qualities. It is our gratitude that we have got a Guru. When you have someone to guide you think it is God's grace.`

  41. तस्मिंस्तज्जने भेदाभावात् ।
  >`Mahapurusha - His people - Tasmin Jane - No difference between God and his people. - Bhed abhaavad. `
  42. तदेव साध्यतां तदेव साध्यताम् ।
  >`Then everything will follow automatically.`

  43. दुस्सङ्गः सर्वथैव त्याज्यः ।
  >`Avoid the Mantharas of the world!`
  >`If you do not get the great souls, never get to the side of bad company. Always avoid them. Even if you are good, shun bad company. If there is no amount of bad in you are a realized soul. But if you are not a realized soul, the bad company will spoil you. And soil you. `

  44. कामक्रोधमोहस्मृतिभ्रंशबुद्धिनाशकारणत्वात् ।
  >`Desire, Anger, Smriti spoil, Buddhi nasha - Gita, if you be in the company of bad people. Kaikeyi was a good person to start with. She loved Rama more than Kaushalya. By the company of Manthara, the selfish desire is risen in her, and then the series of downfall happened. Dasratha dies. Hell breaks loose. How that is solved? Presence of Mahapurusha - Vashistha and Bharata. Vashistha tells Bharata that it is your duty to become the king. `


  45. तरङ्गायिता अपीमे सङ्गात् समुद्रायन्ते ।
  >`Dont think that you are 100% good. Some portion of Kaama, Krodha is remaining in you. As ripples - Taranga. That will become an ocean, by evil company - Apime Sanghat.`
