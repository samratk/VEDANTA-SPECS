  <h5> द्वितियोऽध्यायः - परिच्छेद 3 - प्रेम पराकाष्ठा </h5>
Love reaching its height

---
  51. अनिर्वचनीयं प्रेमस्वरूपम् ।
  >`This is abhichinnam anurag is beyond description. You cannot catch it in words. Not to be described, the real nature of Prema - the abhichnnam anurag`
  52. मूकास्वादनवत् ।
  >`There are many wonderful things in life which cannot be described. Like taste of sugar cannot be described. To speak by words it requires a sufficient amount of "I" consciousness. When I goes away, only love remains. It is like a dumb man in love. Words have gone with that I consciousness. `
  53. प्रकाशते क्वापि पात्रे ।
  >`Kwapi - rare. The love shines in the rare person. By his action, non action we see love of God. It cannot be described by words.`
  54. गुणरहितं कामनारहितं प्रतिक्षणवर्धमानं अविच्छिन्नं सूक्ष्मतरं अनुभवरूपम्।
  >`This love of God is not colored by the Gunas of world. Kamana - tasting God. Not to become God. It grows every moment. The pull in his experience grows every moment. Abhichinnam - it is not obstrcutred or broken. It is continuos but incrases. It never decreases. Beyond mind - Sukshma Taram. `
  55. तत्प्राप्य तदेवावलोकति तदेव श‍ृणोति तदेव भाषयति तदेव चिन्तयति ।

---
