<span style="color:; font-family:Menlo, Consolas, DejaVu Sans Mono, monospace; font-size: 12pt ;">

<h4> || त्रितियऽध्यायः - गौणी भक्ति:|| </h4>
  <h5> त्रितियऽध्यायः - परिच्छेद 1 - गौणि भक्ता: प्रति </h5>

---
>`56. गौणी त्रिधा गुणभेदाद् आर्तादिभेदाद् वा ।`
`GAUNA BHAKTI`
BG 7.16 - Only virtuous people of 4 types worship me
These are the examples of Non Nardiya Bhakti - These are not Naradiya Bhakti .
- Guna Bheda [unreal]
  - Sattvika bhakti - Sits for worshipping God. Alpa Santusta. He is satisfied with any small thing. This is not Naradiya Bhakti. He does not have a path going forward. He is alpa santushtha in spiritual efforts too. Naradiya Bhakti has to grow everyday.
  - Rajo guni bhakti - Durga Puja in Kolkata.
  - Tamoguni Bhakti - does not cleans himself. Lazy.
- Arta - miserable [unreal]
    - Good deeds - punya - you come to God. - Not real  bhakti
    - the misery makes him intensely think of God. Intensity is in Arta Bhakti. So, he is placed above jigyasu.
    - Dukh mein sumiran sab kare
    - They forget God when things are good.
- Jigyasu - For the sake of knowledge he becomes bhakta. It is not natural. [unreal]
    - Kalidasa example.
    - His bhakti is mild. When he gets the knowledge he is likely to forget.
- Artha Arthi - Desirous of money and Bhoga enjoyment. [unreal]
  - Business man. Shubh Laabh. 2 hours in puja. You cannot beat him. They are not bad at all. But that is non Naradiya.
  - Filled with desires.
  - Tirupati Balaji
  - Ananth Padnabhana Temple
  - Shirdi Sai

>`  57. उत्तरस्मादुत्तरस्मात् पूर्व पूर्वा श्रेयाय भवति ।`

Uttar - that is coming later. Purva Purva that is which is coming earlier.
1. Sattva Bhakti -
   - If he gets a proper Mahapurusha, he can easily be moved to Real Bhakti.
   - Max muller brahmins who did not want any parapharmelia. If a Mahapurusha comes he can easily get real Bhakti - Vishay tyag, Sangha Tyaga.
2. Rajo Guni Bhakti -
   - His dynamism is being shown in the world. If a Mahapurusha comes in his life and tells him to use all his dynamism to realize God.
3. Tamasik Bhakti
   - Dacoit bhaktas -  has the guts. They can commit any good or bad thing.
   - Holy Mother traveling along from JaiRam Bati to Dakshineshwar. In the big field there is a Kali Temple. So, people cross in group on day time. The docoit came and was later converted to Bhakta.
   - They do whatever, but before doing that they worship God.

`QAULITIES OF GAUNI BHAKTI WHOM NARADA WANTS TO CONVERT TO REAL BHAKTI. KEEP WHAT YOU HAVE.`
>` 58. अन्य मात् सौलभं भक्तौ ।`

Bhakti is natural or it has come to you due to the above conditions. There are good qualities in your Bhakti even if it is Gauni. This is the easiest. For those in whom it has become natural.

>`59. प्रमाणान्तरस्यानपेक्षत्वात् स्वयं प्रमाणत्वात् ।`

Philosophers require so many arguments to prove the presence of God. A karmi has to prove God. A Bhakta does not have to prove God. Any other proof is not required. I love God. If there is no God, how do i love him?! I am proof for that. I am hungry. That means there is something called food. Otherwise I will not have desire to have food. In my stomach there is a place where food can be dropped. I have mouth to eat the food. If I love God, it means three things -
1. God is There
2. I want Him
3. There are means to realize him.
These are common in all the people - Gauni or Real. They are sometimes confused by others. But this is there. So, your Bhakti is of great value.
Natural Bhakti without any condition. Whom else to pray?

- Love Vs Fear of God!
  - This is Tamoguni Bhakti. Instigated by fear. Moresover, that proves God.



>`60. शान्तिरूपात् परमानन्दरूपाच्च ।`

The most peaceful and supernatural joy times of your life are the times when you spend time for Him. Were they not the most peaceful? Paramananda Rupa. So much Joy you had when you were involved with God. The joy is not sensuous Joy. Paramanananda - This joy is of a higher quality. Moments of Joy in your life are only those when you were thinking of God.

`NARADA WANTS TO CONVERT TO REAL BHAKTI. INSTRUCTIONS TO CHANGE`

>`61. लोकहानौ चिन्ता न कार्या निवेदितात्मलोकवेदत्वात् ।`

न तद असितो लोक व्यावहार
1. Surrender yourself to God.
2. Surrender all your secular activities to God. [laukika]
3. Dedicate all your sadhana to God.
4. Practice self control
5. Do not be anxious of the loss in the laukika world. One of the main obstruction is that people think lauka hani will happen if we are devote to God. Vivekananda says that if you are devoted to God, you might have little damage to laukika. But if you are not devoted to God, you will lose world as well as God.

>`62. न तत्सिद्धौ लोकव्यवहारो हेयः किन्तु फलत्यागः तत्साधनं च ।`

फलत्यागः - It is a mental input that I do not need result. Do not think about the fruit of that work.
When you have got real bhakti, then that is full time job for you.

>`63. स्त्रीधननास्तिकचरित्रं न श्रवणीयम् ।`

1. We should not hear the nature of women. A fickleness is ascribed to women. Dont read Arabian nights. Marrying many women. Man or woman's fickless. Dont even listen to them. That will make bad samskaras in you. If you do not want to eat fish do  not go to fish market. That fish market will build a samskara in you for fish.
2. Do not listen to the charitra of wealthy man.
3. Do not listen to Nastika man. Highly moral but does not believe in God.

>`64. अभिमानदम्भादिकं त्याज्यम् ।`

BG 16. - Aasuri sampad.
1. दम्भ - austentacious. showing off more than that you have.
   - Showing off more humility than you have.
   - Showing off more wealth than you have.
   - Austentation - military person in Chandigarh.
2. दर्प - stench - bad smell of your pride
3. अभिमान - Your pride in your own self - your mind, your body, your intellect
4. क्रोध
5. पारुश्य - rudeness in dealing with others.
6. non devotion

>`65. तदर्पिताखिलाचारः सन् कामक्रोधाभिमानादिकं तस्मिन्नेव करणीयम् ।`

Having dedicated all your actions to God you can direct all your emotions and desires to God.
In every man these things are natural. The asuri Sampatti - indriya pradhanata.
Kaam - Desire and Lust. Turn it to God. Mor ghuriye dao. Excessive is asuri sampathi. That has to be removed. And the natural aspects are to be turned to God.
Sulking - abhimaan - krodha. - towards God.
Anger towards the obstruction to the path to God.
BG 16.2
>`66. त्रिरूपभङ्गपूर्वकम् नित्यदास्यनित्यकान्ताभजनात्मकं प्रेम कार्यं प्रेमैव कार्यम् ।` *MOST IMPORTANT*

1. Nitya Kanta - Nitya husband - madhurya bhav. All people in the world are God's wives.
2. Nitya Dasya - Throughout our life. Throughout all actions and endeavors.
3. Nitya Sakhya - Everything in life is a drama, play.
4. Nitya Vatsalya - Treat everyone as your child. If God can be your child, entire universe is your child. - give, give, give. Don't ask anything in return.
5. Nitya Bhajana atmakam - Express the love of God in your deeds and expressions. Of the form of Bhajan. It should pervaid your life and humanity.

This will cause love to Grow. This will lead to `Tri Rupa Bhanda purvakam.` When love grows, self goes out. It purifies me. In every relations, the same God relation pervades and the love grows immensely in an onepointed manner. I forget at times that I am the son and God is the mother. Love remains. The form should break down.  Relationships vanish. Only love remains.

`Tri-Roopa - I, Love, God.`
These three forms break down. Love remains. I am the son, and I am loving God in a particular way, goes way. This I goes away. God form goes away. Only love remains.

This is the time when the Gauni Bhakti  breaks down, and that leads to the real Naradiya Bhakti.
Prema is the identity that is already their within us. It grows under certain conditions.

---
