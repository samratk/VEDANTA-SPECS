<span style="color:; font-family:Menlo, Consolas, DejaVu Sans Mono, monospace; font-size: 12pt ;">

  <h5> त्रितियऽध्यायः - परिच्छेद 3 - भक्तान्प्रति </h5>

---
>  74. वादो नावलम्ब्यः ।

Dont enter into debate. You cannot argue and make people believe in God. Unless he grows he will never understand. People's arguments and logics are different. By argument you cannot prove the existence of God. You can only live your life.

>  75. बाहुल्यावकाशत्वाद् अनियतत्त्वाच्च ।

There is a scope of different elements and opinion for every point in this world. So dont argue.
You cannot control them. Everybody goes in their own way. Nothing can happen.

>  76. भक्तिशास्त्राणि मननीयानि तदुद्बोधकर्माणि करणीयानि ।

One should read the Bhakti Shashtra and engage into work that enhances their Bhakti. Karma should be used to awaken Bhakti.

>  77. सुखदुःखेच्छालाभादित्यक्ते काले प्रतीक्ष्यमाणे क्षणार्धमपि व्यर्थं न नेयम् ।

Kal or mrityu death waits to pick the person out, irrespective of sukha, dukha, ichcha, benefit etc. So we should not waste even half a second unnecessarily, and work towards enhancing our Bhakti.

> 78. अहिंसासत्यशौचदयास्तिक्यादिचरित्राणि परिपालनीयानि ।

Ahimsa, Satya, Soucha, Aastikya, character values need to be practiced without fail.

> 79. सर्वदा सर्वभावेन निश्चिन्तैर्भगवानेव भजनीयः ।

Being nischint, we need to
  80. सङ्कीर्त्यमानः शीघ्रमेवाविर्भवत्यनुभावयति भक्तान् ।
  81. त्रिसत्यस्य भक्तिरेव गरीयसी भक्तिरेव गरीयसी ।

---
