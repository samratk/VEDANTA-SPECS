<span style="color:; font-family:Menlo, Consolas, DejaVu Sans Mono, monospace; font-size: 12pt ;">

<h5> त्रितियऽध्यायः - परिच्छेद 2 - मुख्य भक्त महिमा </h5>

---

> 67. भक्ता एकान्तिनो मुख्याः ।

Mukhya are real Bhaktas. Ekantino - They know only one thing - God. Their everything is God. They know nothing other than God.

>  68. कण्ठावरोधरोमञ्चाश्रुभिः परस्परं लपमानाः पावयन्ति कुलानि पृथिवीं च

Kanta varodha - Throat is choked.
Romach - horrifilation in the body.
Ashrubi - Prema tears of joy and love.
Prasasparam alapa - Talking to each other.
These mad people make their family holy. And makes the world holy.

>  69. तीर्थीकुर्वन्ति तीर्थानि सुकर्मी कुर्वन्ति कर्माणि  सच्छास्त्रीकुर्वन्ति शास्त्राणि ।

Teertha - a person, a place, a nature, a river, - when one feels he can cross the world. What is special in Teerta? In Kashi? In Ujjaini? Because people from beginning of the time visit these places. Mahapurusha Sannidhya.

>  70. तन्मयाः ।

Filled with God.

> 71. मोदन्ते पितरो नृत्यन्ति देवताः सनाथा चेयं भूर्भवति ।

When they make mukhya bhakta, even the forefathers rejoice. Devta - chethan pradhan - dance out of joy seeing such a real Bhakti.
Sanatha - with a guardian. Mukhya Bhaktas protect the world.

>  72. नास्ति तेषु जातिविद्यारूपकुलधनक्रियादि भेदः ।

Teshu - among them. in their eyes. Amongst them there is no difference between caste, learning, roopa, family, rich or poor, occupation. These differences do not exist among the Mukhya Bhaktas.

  73. यतस्तदीयाः ।

---
