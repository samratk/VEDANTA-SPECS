<h5> प्रथमोऽध्यायः - परिच्छेद 3 - भक्ति निर्णय: </h5>

---

25. सा तु कर्मज्ञानयोगेभ्योऽप्यधिकतरा ।
>`Sa Tu - The real Bhakti - from second sutra - Sa Tu param prem roopa. He is comparing real Bhakti is with other so called Bhakti. Keep aside your idea of bhakti and know what is truly Bhakti. Karma, Gnana and Yoga - Bhakti is more superior. Bhakti is a force. The starting point is purity of the mind, an pull of God. All of us are potentially divine. So, the real nature has always a pull, whether we are aware or not. All our attractions and repulsion in the world are reflections, sometimes distorted reflection of the same pull with which God is pulling us.`
![](Bhakti-Karma-Yoga-Jnana.jpg)


>`Karma is a means to purify our mind. If you purify your work, the work will purify you. There is a time when Karma is not required. It leads to Bhakti. If Karma gives chitta suddhi enough -> result - Bhakti. Karma means I am the doer. It is about interaction between subject and objects. Efforts to meditate - Karma `

>`Jnana - change of consciousness is Jnana. I am a jiva who is living in the world of  objects => Reality is one without a second. Satchitandnad - and that appears are different 'I' in myriads of names and forms. So, they appear as many. Firs step is move from body to consciousness. And then consciousness has to be aligned with this. Gnana is the destination of Karma after purifying. Janana - later half of the path. Karma is the orignal part of the same path. The more we know through Jnana the nature of God, more we are pulled strongly. 'i' is needed Janan. In Bhakti, I is not needed`

>`Yoga - PatanjaliYoga is incomplete. It leads us to Gnana. Yoga supresses the lower faculties of the man. In Gita Yoga is Tat Prapti Upay. That is always higher than Karma, Gnana, Tapsya, Karma. But the result of these lead to Bhakti. I has to live in Yoga. `

>`Bhakti is the ultimate which leads to the ultimate God realization. There is no difference between Gnana and Bhakti. The more our consciouness changes (Janan) the more pull we feel (Bhakti). The more Bhakti, the more we know about God - Janan - more God consciouness. These two are two faces of the same coin. Yoga and Karma is disposed off later. Meditation happens with pure mind - Bhakti`

26. फलरूपत्त्वात् ।
>`Real Bhakti is the result of the path of Karma, Gnana, Yoga. Bhakti is the nature of God. It is the bliss that never goes. Ananda Swarup Bhagavan is the Bhakti. Mind is pure and pull is felt. We dont love God. We cannot love God unless God is pulling us continuously. All shadows pull to the reality. When we feel God is loving us, real progress is starting.`

27. ईश्वरस्याप्यभिमानद्वेषित्वात् दैन्यप्रियत्वात् च ।
>`In love of God 'I' goes away. Dainya - I am not there. Only God is. Nature of God, itself leads to real Bhakti. The nature of Bhakti does not allow ego to arise. When ego subsides nothing but Bhakti arises. Bhakta likes lowliness. In Gnana 'I' is there. In Bhakti 'i' has not place. When you feel I am loving God, it is God is loving you. When this realization come, Narada Bhakti comes. Bhakti can start from any place. As 'i' comes love is blocked. You cannot say the same thing for Jnana. There 'i' is needed. First develop your healthy ego then think of eradicating ego. In knowledge ego is required. ego obstructs Bhakti in every step. What obstructs Bhakti is 'I' and 'mine'.`

>`Example -  Naga Mahashaya - disciple of Thakur. He would always say i am nothing. It obviously means God is everything. Danya means that. Lane of love is very narrow. If you are there, God is not there. If God is, you are not. `

28. तस्याः ज्ञानमेव साधनमित्येके ।
>`Gnana and Bhakti are very close to each other. Gnana is the only sadhana of Bhakti. Gnana alone is the sadhana of Bhakti. Knowledge leads to Love of God.`
29. अन्योन्याश्रयत्वमित्यन्ये ।
>`Bhakti leads to Gnana. Gnana leads to Bhakti. They are mutually supporting each other. `

30. स्वयं फलरूपतेति ब्रह्मकुमारः ।
>`Narada refers to his Guru. Brahma had 4 sons - Manas Putras - Narada has learnt all his knowledge by Sanat Kumar. Let us hear what my Guru Sanat Kumar has told. He has said, Gnana Bhakti are their each seeds and fruits. Both are same and they are the fruits of themselves. A little Gnana grows, and it grows and it grows, it gives to Bhakti. A little Bhakti grows, and it keeps growing and we get Gnana`
>`Gnana phalrupata. Gnana is phalrupata. Jilebi example. Ananda - sugar syrup. Jilebi entering syrup - Janana. Syrup entering Jilebi - Bhakti`

`EXAMPLES : `

31. राजगृहभोजनादिषु तथैव दृष्टत्वात् ।
>`1. Raja - song of a king is king. At the birth or childhood, he was taken care of tribal people. He did not know he was a Raja. At a later stage he came to know he is Raja.`

32. न तेन राजा परितोषः क्षुच्छान्तिर्वा ।
>`1. He did not become Raja by his efforts. By his wars. By his claim. He became Raja because he was Raja. This is swayam phalarupata. You are getting Gnana because God is real nature. If God was not your real nature, you would not have been pulled to God. And due to this pull you will know the real nature. All efforts have to be stopped. All our efforts are needed to feel the pull of God. To know that all efforts are not necessary. `

>`2. You feel relief because you have come back to your own house. Paritosh - satisfaction. The amount of satisfaction is very high. This is not because you went out for years. But because you have come back to your own house. God is pulling you, and you will come to Him, not due to an effort. What you get out of effort, it is due to Karma. Karma ends with karmafala, and stops.`

>`After eating food leaf is thrown away. Be like that thrown leaf. Like Chaitanya Vayu, flow where it takes you. YOu have to float into that storm. Give up your I. That moment you realize God. All self effort is actually to destroy the self. Destroy I. The hunger is like a disease. Food is like a medicine. `

>`Bhakti is 5th Purushartha. Dharma, Artha, Kama, Bhakti`

>>Give Me Strength - Tagore poem.
This is my prayer to thee, my lord—-strike,
strike at the root of penury in my heart.
Give me the strength lightly to bear my joys and sorrows.
Give me the strength to make my love fruitful in service.
Give me the strength never to disown the poor or bend my knees before insolent might.
Give me the strength to raise my mind high above daily trifles.
And give me the strength to surrender my strength to thy will with love.

33. तस्मात् सैव ग्राह्या मुमुक्षुभिः ।
>`Sa eva - Sa - 2nd sutra - 25th sutra - Narada's Bhakti - dont go after other types of Bhakti. If you want Mukti. Freedom is that goal of life. You have to follow this real Bhakti.`


`This bhakti is not to be done. This is a natural state of being that needs to be achieved. We can remove the obstruction, the attraction will automatically come.`
---
