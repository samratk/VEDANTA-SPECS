<h4> प्रथमोऽध्यायः - परिच्छेद 1 -  `भक्तिस्वरूपम्`- Inside flow of the Bhakta</h4>

---

1. अथातो `भक्तिं` व्याख्यास्यामः ।
2. सा तु `अस्मिन्` `परम` `प्रेम` `रूपा` ।
>`Bhakti is the innermost reality, which is bliss always. Tremendous love towards that reality. It is strong pull. Due to impurity of mind we do not feel the pull. Obstacles are only me and mine. When we feel the pull, it will appear to us that roopa. The Bhakti has two sides - the within immortal love. And also the outside form. This inside love illumines the world outside in love. What is pure and true is that inside love`

3. `अमृत` `स्वरूपा` च ।
>`The real nature and form of that love within is immortal, never changing. Never shifting. Never dyeing. In finite thing there is no happiness. Only in this immortality of the spirit, there lies happiness which is permanent. That is Amrita. Our own reality is of nature of ananda. So we feel the pull. Anand and Prem are not different. We are pulled into that Joy that it is within us. That pull is the Prem. This pull takes us beyond the obstruction.The following happens to him who has got this Amrit.`

4. यल्लब्ध्वा पुमान् `सिद्धो` भवति `अमृतो` भवति `तृप्तो` भवति ।
>`With this love is gotten - Siddhi - perfect nature, Amrit - Divity, Tripti - All wants are satiated. There is no two. But one. All desires, trishna comes from that feeling of separateness of feeling of two isntead of one, and that cravin for uinon. But in this love, that separateness is melted away as the love is one within`

5. यत्प्राप्य न किञ्चिद् `वाञ्छति` न `शोचति` न `द्वेष्टि` न `रमते` न `उत्साही` भवति ।
>`Since there is no two, but one, and there is no gap, there is nothing that is craved for. There is nothing that is expected. There is nothing that is hated. There is no one with whom one is infatuated. There is no worldly pursuits that one is excited or enthused for. `

6. यत् `ज्ञात्वा` `मत्त:` भवति `स्तब्धो` भवति `आत्मारामो` भवति ।
`Intoxicated with God, we turn inside, and become silent`

>`After knowing this the person becomes intoxicated of love and is totally lost in his ownself - God. Gyatwa means realizing which, changing our direction of attitude and consciousness. This intoxication if real normal. Thakur used to be intoxicated in love, and was not able to put his steps properly. Kali Bhaavamrita he has drunk. It ends with Nirvikalpa samadhi. No matter with whomever he is with, or he is alone, he is intoxicated and totally immersed in atman - Atmaramo!`

7. `सा` न `कामयमाना` `निरोधरूपत्वात्` ।
`Mind is internalized in Bhakti`

>`This love within is different from the worldly Kaam. This infact stops the wordly pursuits altogether. This pull cannot exist in the form of worldly desires. There is an inner check of the outgoing tendencies. The mind becomes to strong within by the pull of God, it cannot go out. Exeternalization of the mind is totally stopped if there is real Bhakti. This might appear to be repression or suppression! We will shattered into pieces if everything this free. The outgoing tenedencies of mind is being checked everytime. The following way`

8. `निरोध:` `तु` `लोक` `वेद` व्यापार `न्यासः` ।
`Everything is positioned to, and through God`

  >`Although it stops the externalization of the mind, it puts in right place the mind with respect to the secular and the spiritual practices of everyday life. For every work the attitude is that I am an instrument of God. And all work is offered to God. Nyasa - putting in the right place, having renounced the wrong place. Our secular and religious and spiritual sadhana - i am doing sadhana is not the right spirit. All these activities are dedicated to God. From the field of I to God. God is work through me. This is called the nirodha. We are not suppressing or repressing anything. If God is doing everything, mind will not go anywhere.`

9. तस्मिन्ननन्यता तद्विरोधिषूदासीनता च ।
`Everything is God. Ignore everything else`

  >`1. In Him lies the Ananyata. Rama, Krishna, Shiva, I - we are not separated from God. There is nothing in the world that is separate from God. This is Ananyata. This is giving us Nirodh. If there is nothing in this world, that is not God, why should my mind go to this or that object? Ananyata leads to Nirodha. Neither I or anything that is mine, or anything else that exists is different from God. Other than God, there is nothing in this world. मेरा मुझको को कुछ नही. But there are many forces in this world that opposes this - Virudhisu - don't bother about that. Upeksha - Udaseenata - Ut Asinatha - Sitting above. Ignore bad thoughts. If you think of the bad thoughts, it will become strong. Dont quarrel with them. Ignore them and think of God. How this anandata is achieved? How to practice tad virodhi udasinata?`

10. अन्याश्रयाणां त्यागोनन्यता ।
`Depend only on God - our own reality`
  >`Let nothing be the support of your life. Never think this is my support. Bank balance. House. Instution. If you take anything as your support other than God, you cannot achieve the Bhakti. Tyag all Ashrama! Pakka Bandobasta kar lena chahiye. You should have a firm arrangement - Putting your mind in God. `

11. लोकवेदेषु तदनुकूलाचरणं तद्विरोधिषूदासीनता ।
  >`while living in world, one can practice udasinata, but at the same time find out something anukul (favorable to Bhakti or God), amongst the opposing forces within and outside, and do that which brings you near God. Then you can transcend teh virudhi forces. This is a challenge in our intellect to find out that anukul in both secular and spiritual matters. `

12. भवतु निश्चयदार्ढ्यादूर्ध्वं शास्त्ररक्षणम् ।
  >`Let there be. Conviction. Tadya - form. Urdhya - after. After your conviction is firmed, follow the Sashtra. Conviction is not realization. During this time, we should not stop sadhana. We should follow sadhana and sashtra. Let there be shashtra rakshanama. Unfailing guidance to further progress. At this point you are fit to follow the Shashtras and you should follow the shashtra. Bridharanyaka Upanishad - Yajnavalakya says Brahman is that which is beyond any relativity. First thing is that you will lose your desires when you know Brahman. You become Pandit - who gets the Panda atma visaya buddhi - our think based upon Atman. Your mind, body is permeated with Brahman. You become balya after that. Childlike egolessness. Knowledge is then absorbed more. Then you get Maunam - meditative nature of mind. Then you become Brahmin - knower of Brahman. Then he lives as  he likes. What he does is Moral. He is beyond all rules. After realization caution follows him. he does not follow caution.`

  `Buddi/Conviction Vs Realization - `
  >`Conviction is in buddhi. Realization is first practically realized. Philosopohy and experiences are two different lines. Need not be that first we understand and then we realize. Buddy is a scavenger. First Buddy experienced and then intellectualized the experience. Intellectual conviction makes us wrongly believe we have realized incorrectly. Or sometimes we forget to realize.Heart is higher than Mind. Gopaler ma, and many other devotees never has an intellectual conviction to start with.`

13. अन्यथा पातित्यशङ्कया ।
  `Dont mortgage yourself to anything or anyone else as support`
  >`By chance or luck you might reach the goal. But there are chances of falling down. There are tendencies to fall down if you do not follow the Shashtras. In Kolkata there is a fort willilam. Many stories below the fort. We do not feel we are slopping down, as the slope is less. You will fall down not knowing the gradual downfall, if you do not follow Shashtra.`

14. लोकोऽपि तावदेव भोजनादि व्यापारस्त्वाशरीरधारणावधि ।
  >`Loko api - social rules - How long should we follow the social rules? Social rules help a man not to fall down. It checks the man not to become animals. Social rules do not help you to move up. Upto the point you are falled into the current of Bhakti, you need to follow your secular laws. When you float in the current of Bhakti, and are pulled by that innermost reality, throw them out.`

  >`How long we will have to follow the dietary rules? needs of the body and work. So long you are holding up to the body, you need to follow the needs of the body. A man in highest sense of realization does not care of the body. That has nothing to do with how much Bhakti you have.`
