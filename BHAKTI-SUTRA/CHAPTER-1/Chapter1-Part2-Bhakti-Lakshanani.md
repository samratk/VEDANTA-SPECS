<h4> प्रथमोऽध्यायः - परिच्छेद 2 - `भक्ति लक्षणानि` - Outward symptoms of Bhakta </h4>

---

15. तल्लक्षणानि वाच्यन्ते नानामतभेदात् ।
>`Bhakti are spoken or expressed in variety of ways. This is because based on different minds of every person. Bhakti is same. But the thoughts and expressions are different by different people. This sutra prepares us to say that outward symptoms are many. The source Bhakti is one`

16. पूजादिष्वनुराग इति पाराशर्यः ।
>`Parashara's son is Vyasa. He says, through puja etc, Bhakta's love will be expressed. 1. Brightest example we know is from the life of Thakur's father. When he was in difficult situation, and he goes back home with Billa patra. Love for God. It is not love for pujar. Love for God is expressed as his puja. 2. The story of the Jogari. Old man, falls down and after three days again same. When her really become bed ridden. At that time, he was unable to do. So, he stopped. His anuraga is for God and that is expressed through this activity. `

17. कथादिष्विति गर्गः ।
`Kirtan & Shravan`
>`Through Katha his anuraga for God flows. Narayanana Narayana katha of Narada. Talking about God. Whenever Rama's name is sung, Hanumana comes there. He is not particularly addicted to Rama. He was able to realize the goal of his life through the name and form of Rama. Narada for singnig , Hanuman for listening. Shravana is a form of Bhakti.`

18. आत्मरत्यविरोधेनेति शाण्डिल्यः ।
`Pull towards one's own real self`
>`If you are involved in puja you may become extrovert. You can by mistake get involved in puja, instead of through puja. Puja should not be in the cost of Atma Rati - love for the innermost reality. If your activities opposes to this pull to the innermost reality, then it is not right. Atman is inside does not however mean that Atman is not outside.`

19. नारदस्तु तदर्पिताखिलाचारता तद्विस्मरणे परमव्याकुलतेति ।
`Narada's comprehensive & concrete definition - habit of doing everything for Him. आचरण:`

>`Tu is not contradiction here. लाचारता - Habit/Nature - Having the characteristics of this type of achara - having dedicated all our action to God. This is not abstract. This is concrete`

>`तदर्पिताखिलाचारत - Anybody else who can think or feel - whatever he is doing is for the sake of the Lord. Even upto the extent you go to the toilet, take bath, take food - for Him - Because I love Him. I live for Him. I love Him. Whatever I do is for Him. I am learning NBS for Him. The characteristics and habits - the man is become for Him - That has become his habit. Whether breathing, sleeping, running, moving, standing, talking, sitting, working, - everything for Him. So, automatically the selection of how we do all these will change, because it is for Him. There is no I. I is dedicated to God.`

>`तद्विस्मरणे परमव्याकुलतेति - In the case of having spent time wihtout Him will cause immense Vyakulata.`
>`Poha offered to Sri Thakur and Thakur Ma. Swami Pitambarananda Ji has a strong urge to eat more. The act of cooking breakfast or tea was dedicated to Him.`

>`We forget, and then the real Bhakti weeps if they spend time without Him. Emotion is different from feeling. Emotion is biological. Feeling is deeper - Divine - beyond body. In the beginning you can assume the Bhaava is emotion. Then try to temper it using Philosophical and practical reason. If it is emotion it will go away. Emotions can be checked by philophical or practical reason. Feeling does not go away. It comes from the absolute tranquil heart. Vyakulata and emotions (Udvega) are totally different. Shallow mind totally gets thrown out by this Vyakulata. This is the pull that we feel from inside. There is no reason. There is no rationality. God is swayam Brahman. No proof is not required. `

>`Lecture - real challenges for Vedanta. Reason however strong, can never replace the man's real experience. Emotion/feeling - is closer to real experience. Gnana and Bhaava meets and causes Upanishads. Feelings in deepest heart is much closer to reality than reason. Reality is pulling us. In pure mind the pull is felt.`

20. अस्त्येवमेवम् ।
>`Can this happen? Can a person become so vyakula with little forgetfulness? Yes- there are many examples exist. Like this like this..It is not a strange thing. Assertion in reply to our question.`

21. यथा व्रजगोपिकानाम् ।
>`Like the Gopika's of Braja. Braja means - walking place, land for the sake of cows to graze upon. Pasture land. Gopi thing is mis-understood many, who want to criticized Gopi and Krishna.`
22. तत्रापि न माहात्म्यज्ञानविस्मृत्यपवादः ।
>`In Gopi's case never mistake the deep love of Gopikas for Krishna. It was not a yaari - love outside wedlock. Krishna is not Para Purusha. He is Parama Purusha. Who feels the whole world, and transcends the world. It is the Mahatma Gyan, that differentiates. The love for a paramour -Tasmin - does not contain happiness in the happiness for the beloved. Attraction is for your own pleasure. Love for supereme being lies in the pleasure in the pleasure of God. Gopi's were weeping when Krishna left Brindavan for Mathura. Gopi's said I am Krishna, and they became Krishna. Krishna comes. Then Gopis angers on Him. He says three types of love - 1. You make me happy, I will make you happy. 2. It is for my happiness. 3. Love of mother. What love? Krishna says, I will give pain to those who love me so that they become free from impurities. Prasadya - Be prepared for this. You love Him. He will give you trouble.`

>`Krishna and Arjun is invited by a rich man. While coming out Krishna says, may your property increase. Cowboy - let your cow die. So that he comes 100% to God. That is God's love.`

>`GRACE MARKS JOKE!. It is the grace of God who can give you the nectar of Bhakti. You cannot get it by your efforts.`

23. तद्विहीनं जाराणामिव ।
>`Bereft if that knowledge, we will love like an illigimate love. Jara - paramour. `
24. नास्त्येव तस्मिन् तत्सुखसुखित्वम् ।
`Being happy in the happiness of beloved. Physical attraction is for selfish happiness. Tat Sukhi Tuam is not in physical attraction.`
