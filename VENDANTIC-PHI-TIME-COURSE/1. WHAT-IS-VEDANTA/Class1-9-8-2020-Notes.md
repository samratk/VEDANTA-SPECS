<span style="color:; font-family:Menlo, Consolas, DejaVu Sans Mono, monospace; font-size: 12pt ;">

######I. Mandatory Homework for Session 1:
1. Swami Medhananda, “Introduction: The Past, Present, and Future of Scholarship on Vedānta,” in The Bloomsbury Research Handbook of Vedānta, pp. 1-2.

2. Swami Medhananda, “Asminnasya ca tad yogaṃ śāsti: Swami Vivekananda’s Interpretation of Brahmasūtra 1.1.19 as a Hermeneutic Basis for Samanvayī Vedānta,” in Rita Sherma, ed., Vivekananda: His Life, Legacy, and Liberative Ethics (forthcoming), pp. 1-7.

######II. Reflective Questions
1. What are the three possible meanings of “Vedānta”?
   - Upanishads,
   - The Prasthana Traya  - Gita, Brahmasutra, Upanishads.
   - The various sampradays.

2. What is the prasthāna-traya?
   - Shrimad Bhagavad Gita
   - Brahmasutra
   - Upnaishads.

3. What is the etymological meaning of “Vedānta”?
   - The completion or end of Vedas. Clumination of Vedas. Knowledge portion of Vedas. Which talks about highest goal of life - Realization of Brahman.

4. Why did Swami Vivekananda criticize traditional commentators on the Vedāntic scriptures?
   - Traditional commentators stick to their one way of fitting the Upnaishads. Upanishads are more diverse than what they say.
5. What was Swami Vivekananda’s alternative method for interpreting the Vedāntic  scriptures?
   - Learn from the scriptures directly. And then read commentators. Then use your own mind to restrospect what exactly the shashtras are saying.
6. Why did Swami Vivekananda call the Bhagavad-Gītā “the best commentary we have” on the Upaniṣads?
 - Krishna synthesized all the paths in the Gita. They can all directly lead to Moksha. Take up the path that works for you. 

######III. Class Notes
1. What is Vedanta
  - Started with Upanishads
  - Later contained the Brahmasutra and The Bhagavad Gita - Prasthana Traya
  - Also it might refer to the sampradayas - Advaitya Vedanta, Vasistha Advaith, Dvaitha Vedanta, Bhedabheda Vedanta. All of these accept the authority of the Prasthana Traya.
2. Etymological Vedanta
  - Culmination of Vedas
  - Vedanta is a critique of Veda - ritualistic sacrifices are as frail as a raft that can fail at any moment.
  - Vedanta accept the validity of the Karma Kanda and moves forward from there.
  - Karma Kanda - perform sacrifice to attain heaven. - Brahmanas of Vedas
  - Janan Kanda - Upanishads.
  - Dating -
    - Earliest Upanishad - Brihad Aranyanaka Upanishad, Chandogya Upanishad - (800 - 500 BC)
    - Middle period - Tattraiya Upanishad, Isa, Prashna, Aitteriya (300 BC)
    - Katha, - 100 AD
3. Liberation is possible by realizing Brahman or Atman.
4. How we realize Brahman?
   - By renunciation of sense pleasures and performance of spiritual practices.
    - Story of Nachiketa and  Yama in Katha Upanishad - When you run behind sense pleasure, you strengthen your bondage with body and mind and come back. A rare wise person desiring immortality goes inwards and sees the Brahman. Instead of focusing on the outword, but inwards - not as body or mind, but the true nature of Atman. By this you attain the knowlege of Atman. That leads to liberation.
5. Nature of Brahman
   - Impersonal
     - Brihad Aranyanaka Upanishad -  3.9.26
     - Brihad Aranyanaka Upanishad -  2.4.12
   - Personal
     - Chandogya - 4.2.2
     - Brihad Aranyanaka- 6.1.1
   - Personal & Impersonal
     - Svetasvetara Upanishad - 6.11
6. Is Brahman Immanent (Part of the world) or Transcendent (separate from the world)?
   - Immanent
     - Sarvam Khalividam Brahman - Chandogya - 3.14.1
   - Transcendent
     - Mandukya 7
   - Immanent & Transcendent
     - Katha Upanishad 2.2.9-10
7. How do we interpret the Upanishads?
   - Vivekananda - Commentators are conscious liars. They interpret the Upanishad to suit their philosophy. Tricks followed by Advaithins -
     - Adhikari Bheda - Personal good hood -  inferior aspirant - by advaithic commentators.
     - Two levels of reality - Pramarthika (Relative standpoint of ignorance) and Vyavharika (Absolute standpoint of Truth)
8. Vivekannda's preferred method?
   - Both And approach instead of either or.
     - Understand the original meaning of the Scriptures independent from the traditional commentators. Harmonize the Bhakti, Karma, Janana.
     - Brahman (Impersonal God) and Sakti (Personal God) are inseparable. Give equal validity to the personal and impersonal descriptions of the Scriptures.
9. Shrimad Bhagavad Gita (200 BC - 100 CE)
   - Upanishads are Shruti. Vedas are Shruti. Gita is Smriti.
   - Krishna seamlessly harmoizes the immanent and transcendent nature of the Divine.
   - It harmonizes karma, Jnana, Bhakti.
   - This is the best commentary of the Upanishad.
10. Brahma Sutra (300 BC - 400 CE)
  - They systematize the marvelous truths of the Vedanta
  - 555 sutras - extremely laconic and concise statements.
  - Missing nouns, verbs, main words like Brahman are missing.
  - Guru Parampara - the assumption is that there lies a living verbal tradition where Guru explains the context and details of the sutras of the students.
  - 4 chapters - adhyaya. Each chapter is divided into 4 padas. Eacha pada is divided into adhikarans (topics). adhikarana divided into sutras.
    - Chapter 1 - Savamaya Adhyaya
      - 1.1.19 - The scriptures teach the union of this and that. Yogam.
        - Yogam - Sankara - absolutely identity with Brahman. Salvation consists  in realizing our true identity with Brahman
        - Yogam - Bhaskara - Bhedabheda - The school of difference and non difference - Yogam means jiva is simultaneiously different and non different with Brahman.
        - Yogam - Yathirtha (Maadhava Vedanta) - Dwaith -
        - Yogam - Vivekananda -
    - Chapter 2 - Avirodha Adhyaya - defends Vedanta philosophy against possible objectsiona nd criticism of rival philosophical schools like Samkhya, Yoga, Vaisesika, Buddhism, Janism.
      - Vaishyamam Nainyagranya Adhikarana - Partiality and cruelty - given the belief of personal God. 2.1.34-36.
        - 2.1.34 - No because of God's dependence on the law of Karma.
    - Chapter 3 - Sadhana Adhyaya
      - Nature of Brahman
      - Nature of Transmigrations
      - Nature of awake, dream, deep sleep
      - Different upasanasa and vidyas - meditations.
    - Chapter 4 - Phala Adhyaya - Nature of Liberation.
      - V. S. Ghata - A study of Vedanta - Sutras seem to suggest that the liberated soul lives with personal God in the Brahmaloka. Eternally.

###### IV. Q&As of the session
1. Ashwamedha Yajna - ritualistic sacrifices in the Brahmanas. - Chapter 4 of Gita and other Upanishads - They given them a more spiritual and symbolic way. Sacrifice in Chapter 4 - Jnana Yagjna. Japa Yajna. I am the sacrifice of repitition of Gods name. Sacrifice the need of the ego and pleasure seeeking propensities.
2. Dating - Traditional view - Mimansa philosophers - no author of the Vedas. It is the date of being when written down. Let us know be bogged down when it was written down. It always existed in verbal form.
3. Janana & Karma - which is better? - Session 4.
4. What is the difference between Personal and impersonal, and immanent and transendental? Personal God does not mean a man. Personal God means A Theistic God with omnipresent attribute. You can enter a loving relationship wtih that God. In that sense he is personal. He has some omni attributes - ominiscenet, omnippresetn, all loving. One form of the Personal God is Avatara. But it cannot be limited to one man. God can be formless but personal. We should not mix with form and personal.
5. Immanent is not about God is personal or impresonal. It says how it relates to the world. If the world is in the God he is immanent. If He is outside world, he is transcendant. Pan-enthiesm - God is both in this world and beyond the world.
6. You cannot ignore the traditional commentators. You will have to be aware how the traditional commenetators explain them. Only that you cannot rely on one of them and ignore the rest. It is better to be like V S Ghate. You should read the primary texts knowing the Sanskrit. And then read the commentaries. Then you can compare and see what they agree or disagree on and then provide your own interference using your own brain.
7. When Upanisha talks about knowledge of Brahman. They do not considered as the Impersonal Brahman strictly. Ishwar laav is jeevan er uddyaeshho. You do it either way.
8. Bhaja Govindam - it is not necessarily written by Shankara. As per him Bhakti Yoga is just a preparation to know Brahman. If he would have written he would have contrdicted himself.
9. Vyavharika and Paramarthika - My daily life is provisional standpoint - ignorance of the Brahman.

######V. Reference books
For further explanation and reading on this subject you can refer to the following:
1. Sri Aurobindo, “Chapter XVII: Indian Literature—2 [On the Upaniṣads],” in Sri Aurobindo, The Renaissance in India (Complete Works of Sri Aurobindo, vol. 20), pp. 329-341.

​2. V.S. Ghate, The Vedānta: A Study of the Brahma-Sūtras with the Bhāṣyas of Śaṃkara, Rāmānuja, Nimbārka, Madhva, and Vallabha (Poona: Bhandarkar Oriental Research Institute, 1981).

​3. George C. Adams, The Structure and Meaning of Bādarāyaṇa’s Brahma Sūtras: A Translation and Analysis of Adhyāya I (Delhi: Motilal Banarsidass, 1993).
