<span style="color:; font-family:Menlo, Consolas, DejaVu Sans Mono, monospace; font-size: 12pt ;">

######REFLECTIVE QUESTIONS

1. How does Śaṅkara interpret the terms “jñāna” and “vijñāna” in the Bhagavad-Gītā?

How does Sri Aurobindo interpret the terms “jñāna” and “vijñāna” in verse 7.2 of the Gītā?

How might Sri Ramakrishna have influenced Sri Aurobindo’s interpretation of “jñāna” and “vijñāna” in the Gītā?

How does Śaṅkara interpret verses 18.54-56 of the Bhagavad-Gītā? How does he interpret the term “bhakti” in verses 18.54-55?

How does he understand the transition from verse 55 to verse 56?

How does Sri Aurobindo interpret verses 18.54-56 of the Bhagavad-Gītā?

How does Sri Aurobindo understand the transition from verse 54 to 55 and from verse 55 to 56?

According to Sri Aurobindo, how does the Gītā’s conception of the spiritual realization of vijñāna relate to spiritual practice?
