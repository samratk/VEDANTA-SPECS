<span style="color:; font-family:Menlo, Consolas, DejaVu Sans Mono, monospace; font-size: 12pt ;">

###Session 2 - The Major Schools of Vedanta: A Bird’s Eye View
####09/14/2020 6.00pm to 7.30pm Pacific Standard Time

######We will learn about some of the major doctrinal similarities and differences among the major schools of Vedānta—namely, Advaita Vedānta, Viśiṣṭādvaita Vedānta, Mādhva Vedānta, and four subschools of Bhedābheda Vedānta.

Prepare for this Session - please read and reflect on the reading material prior to the session

######I. Mandatory Homework Assignment:
Swami Medhananda, “Introduction: The Past, Present, and Future of Scholarship on Vedānta,” in The Bloomsbury Research Handbook of Vedānta, pp. 2-8

######II.Reflective Questions
1. What are the four major schools of Vedānta (including subschools)?
 - Advait (Sankara, Gaurapada)
 - Vishistha Advait (Ramanuja)
 - Bhedabheda
   - Aupadika Bhedabheda - Bhaskara (non-bhkati)
   - Shudhdha Advait - Vallabha - The Śuddhādvaita subschool of
Bhedābheda upholds the paradoxical doctrine of avikṛta-pariṇāma, the view that
Brahman transforms into the world while somehow still remaining unchanged
(avikṛta)
   - Achintya Bhedabheda - Chaitanya
   - Swavavika - Nirambaka
 - Dwet - Madhava.

2. How do the schools of Śuddhādvaita and Acintyabhedābheda differ from Viśiṣṭādvaita and Dvaita Vedānta in their understanding of the nature of Brahman?
- Visitha Advait & Dwaita do not hold Brahman as nirguna. For them Brahman is purely Personal.
- However Achintya Bhedabheda (Tanubha of Krishna) and Shuddha Advaita (nirguna as lotus feet  of Krishna) holds Brahman as nirguna too. Personal and Impersonal.

3. Which schools of Vedānta consider the world to be real and which consider the world to be unreal?
- World is unreal for Advaita. World is real for all others.

4. Why is Vallabha’s school called “Śuddhādvaita”?
- The Śuddhādvaita subschool of
Bhedābheda upholds the paradoxical doctrine of avikṛta-pariṇāma, the view that
Brahman transforms into the world while somehow still remaining unchanged
(avikṛta). Contrasting with Sankara as incomplete of non dual. Sankara has Maya and Brahman (dual).

5. What is the difference between vivartavāda and pariṇāmavāda? Which Vedāntic schools subscribe to these doctrines respectively?
- vivartavāda - Advaita in Paramarthik sense. World is an illusory appearance of Brahman.
- Parinamavada - Advaita in Vyavaharic sense, Vishistha Advaita, Achintya Bhedabheda. Madva Dwait does not subscibe to Parinamandva.

6. How does the Śuddhādvaita school explain the relation between Brahman and the world?
The Śuddhādvaita subschool of
Bhedābheda upholds the paradoxical doctrine of `avikṛta-pariṇāma`, the view that
Brahman transforms into the world while somehow still remaining unchanged
`(avikṛta)`

7. How does Dvaita Vedānta differ from all other theistic schools of Vedānta in its understanding of the relation between Brahman and the world?
- Dvaita Vedanta rejects pariṇāmavāda. Brahman is the instrumental cause but not material cause.

8. How does Bhāskara’s Aupādhika Bhedābheda differ from Śaṅkara’s Advaita Vedānta in its understanding of the relation between Brahman and the individual soul (jīva)?
- Both believe jīva is identical with Brahman. But is limited and subject to suffering when it is associated with limiting adjuncts (upādhis). Advaita takes these upādhis to be unreal, and Aupadhika takes them as real.

9. According to Acintyabhedābheda, what is the highest goal?
- Bhakti - Supreme love of God. For others it is Mukti. Achintya Bhedabeda also accepts Mukti. (Dhama, Artha, Kaama, Moksha). But Achintya has Bhakti as a place above Mukti.

10. How do Advaita Vedānta and Aupādhika Bhedābheda differ from all other Vedāntic schools in their understanding of the nature of salvation?
- Both claim that in the ultimate salvation, the individuality does not remain. Only Brahman remains.

11. What is the nature of salvation according to bhakti schools of Vedānta like Viśiṣṭādvaita, Dvaita, Śuddhādvaita, and Acintyabhedābheda?
- Go to the Vishnuloka, Vaikunta or Gokola and be in formless body, and serve Vishnu. Soul is in a eternal loving relationship with God.

12. Why does the metaphysics of Advaita Vedānta make it difficult to justify the possibility of liberating while living (jīvanmukti)?
- World is unreal. World and Physical embodiment is a product of ignorance. Brahman realization is eradication of ignorance. If we eradicate ignorance, the effects of ignorance will also be destroyed. Since our physical emobodiment is a product of ignorance, once we are eliminated all ignorance, phsical body cannot exist. So, even Jeevan Mukta has a trace of ignorance. So, he remains in the state of body. once the trace of ignorance is eliminated, they leave their body.

13. Which schools of Vedānta reject outright the possibility of jīvanmukti?
- Ramanuja Vishistha Advaita, and Nimbaraka's Swabhavika Bhedabheda both rejects jeevanmukty.

14. How does Śaṅkara’s understanding of Jñāna-Yoga differ from Rāmānuja’s understanding of Jñāna-Yoga?
- Shankara's Jnana yoga is about listening and reflecting to the Maha Vakyas - our absolute identity with the non dual impersonal Brahman. We should hear them, reflect on them and meditate on them as true nature and internalize them. That should lead to knowledge of non dual Brahman.
- Ramanuja - Vishistha Advaith - he does not accept a different the existence of a non dual Atman or Brahman. It believes that each one of us in an eternal soul separate from the body-mind complex. Janan Yoga is a practice of -
  - Discerning between our eternal soul and the body-mind complex.
  - Meditating on our true nature as jeevatman, as the eternal soul. - Atman Avalokanam - Knowledge of the above point.
  - Once Atmana Avalokanam has been achieved, then continuous meditation on the personal God leads to Bhakti.

15. According to Advaita Vedānta, what is the purpose of Bhakti-Yoga and Karma-Yoga?
- Bhakti and Karma yoga are preparatory disciplines menat for purifying and concentration of mind, and then we leave them  behind and practice Jnana Yoga and attain Brahma Janana.

16. According to Viśiṣṭādvaita Vedānta, in what sequence should Karma-Yoga, Jñāna-Yoga, and Bhakti-Yoga be practiced?
- (Karma + Jnana ) => Atman Avalokanam => Bhakti Yoga => Moksha (Reverse of Sankara.)

17. How do the Teṅkalai and Vaḍagalai subschools of Viśiṣṭādvaita differ in their understanding of how to achieve salvation?
- Tenkalai - God's grace is sufficient to achieve salvation.
- Vedagalai - God's grace + our own effort is needed. 

######III. Class Notes
>1. Causality

1. `The material cause:` “that out of which”, e.g., the bronze of a statue.
2. `The formal cause:` “the form”, “the account of what-it-is-to-be”, e.g., the shape of a statue.
3. `The efficient cause:` “the primary source of the change or rest”, e.g., the artisan, the art of bronze-casting the statue, the man who gives advice, the father of the child.
4. `The final cause:` “the end, that for the sake of which a thing is done”, e.g., health is the end of walking, losing weight, purging, drugs, and surgical tools.

>2. Sampradaya
 - Advait (Sankara, Gaurapada)
 - Vishistha Advait (Ramanuja)
 - Bhedabheda - Bhartri Papancha - earlier than Sankara
   - Aupadika Bhedabheda - Bhaskara (non-bhkati) - contemporary of Sankara - non Bhakti
    - Vaishnava
       - Achintya Bhedabheda / Gaudiya Vaishnavism - Chaitanya
       - Swavavika - Nirambaka
       - Shudhdha Advait (Pushti Marga)- Vallabha - The Śuddhādvaita subschool of
Bhedābheda upholds the paradoxical doctrine of avikṛta-pariṇāma, the view that
Brahman transforms into the world while somehow still remaining unchanged
(avikṛta)
 - Dwet - Madhava - Maadhva Vedanta

>3. Similarities
1. Prathanatraya
2. We suffer because of ignorance of Brahman
3. We are eternal souls
4. We can attain Brahman by sadhana
5. Knowledge of Brahman leads to salvation.

>4. Nature of Brahman
1. Advait is outlier - Only school of Vedanta - Brahman is Nirguna or Nirvishehsa - non dual. The personal God is ultimately unreal. Vyavharica - all exist. Paramarthika - world is unreal and the personal God.
2. All others are theistic. Belief in personal God, with omni attributes.
3. Vishitha Advaith of Ramanuja, Madhava Dwait - Brahman is exclusive personal. They do not accept nirguna Brahman.
4. Shuddha Advaita and Achintya Bhedabeda - Nirguna is minor aspect of Krishna. they believe in Nirguna. Highest reality is all loving Krishna. Some minor aspect of Krishna is advaita Brahman. Krishna's - Tanubha - ephemeral brilliance - is nirguna - Achintya. Shudhdha Advaita of Vallabha - foot of Krishna. Turn the table by saguna is highest.
>5. Nature of the world
1. Is the world real of in empirical standpoint. From ultimate it is unreal.
2. All others  - world is real.
3. Vallabha - Sudha Advaitya - Giridhara named it - Jab at Sankara - Sankara - Maya is unreal. But how can you use that to explain the world? So Vallaba says it is devoid of Maya.

>6. Relationship of world and Brahman
1. Advaita - From ultimate standpoint there is no relationship between Brahman and the world. `Vivartavada` - The only reality is nirguna impersonal Brahman. Brahman is the instrumental and efficient cause of the world from an empirical standpoint. Rope and the snake. Snake is the Vivarta. When I know the rope - reality, i do not see the Snake. Both do not appear at the same time.
2. Vishistha and Bhedabheda - `Parinamavada`  - Brahman or some aspects of Brahman actually transforms into the world. `Sharira & Shariri` - This world is related to the Brahman and at the same time. World is dependent on Brahman for its real existence. Bhedabheda - Brahman at the same time different and same with world and individual soul (jiva)
3. Shuddha Advaita - `avikrata Parinamavada` - Brahman transforms into the world but it does not change.
4. Achintya Bhedavedha - World is a transformation of one particula aspect of Krishna - Krishna Shakti. That shakti is different and same with Krishna.
5. Madhava Vedhanata - Dwet - `outlier` - rejects parinama - There is a fundamental difference between the world and Brahman. Brahman is just Nimmita Karana (efficient) but not material cause. Brahman cannot be made impure from the world.
6. All theistic school agree that Brahman is independent and `Swatantra`. The world is dependent on Brahman. For Sankara world does not exist.

>7. Brahman & Individual soul
1. Sankara - We are same as Brahman. We feel different because we identify with the limiting adjuncts - Upadhis. Those are unreal.
2. Bhedabheda - Jiva is same and different. Jiva are amshas (part) of Brahman. Ocean & Wave, Spark and Fire.
3. Bhaskara - Aupadika - closer to Sankara. The difference is Upadhis are real. Jiva is in essence identical to Brahman. As per Bhaskara Brahman is personal God.
4. Vishistha Advaita Vedanta - Ramanuja - Sharira Shariri. 1. There is an independent connection with Brahman. 2. Jiva is dependent on Brahman.
5. Madhava - Dweta - Jivas are reflections of Brahman - `partibimba`. Jivas are fully real. Relfections are real. Sankara reflection is unreal.

>8. State of Salvation
1. Achintya - `outlier` - Mukti is not the goal. That is Tuchcha Phala. Bhakti is highest.
2. Nature of salvation - Advaitya and BhedaBheda of Aupadhika - no self in salvation. there is only Brahman. Sheer non duality. Vast ocean. It is due to the limiting adjuncts we feel different. State of liberation after fall of body - no individuality. Every other believe that individuality still remains. Salvation is being in super celestial realm - Ramakrishna Loka, Vaikhunta Loka, Goloka - living eternally. Blissfully communing with the PErsonal God. Eating sugar instead of being sugar.
3. Jeevan Mukti - salvation while living or after death?
- Advaitya believes in Jeevan Mukti. But it is difficult to reconcile both. Ingnorance causes physical embodiment. When ignorance is removed due to salvation, body should not be there. Phsyical body is effect of ignorance. So, knowledge of Brahman cannot be possible along with phsycial body. So, even the Jeevan Mukta has a `avidya lesha` - because of praradbdha karma. Jeevan Mukta is not fully realized. Waterdown aspect of Mukti. Lance Nelson.
4. Vishitha Advaitha and Swabhavika Bhedavedha - rejects the Jeevan Mukta. You can at max become Stitha Pragna.
5. Madhva - rejects Jeevan Mukta. but `aparoksha gnana`
6. Achintya Bhedabheda -
7. Ramakrishna according to Jeeavan Mukta - I do not exist. I am the servant of God. it is similar to Achintya Bhedabeda.

>9. Spiritual Practice
1. Jnana Yoga, Bhakti, Karma are all different.
2. Advaitya - Jnana Yoga - focus on Brahma Vakya. Only direct path to liberation. Karma and Bhakti are indirect. They purify and concentrate the mind which makes them competent for Janana Yoga. Two levels of `Adhikari - Manda and Uttam`. Impure mind should practice Karma and Bhakti. After practice of Bhakti and Karma, you become and Uttama Adhikari and becomes fit for Jnana Yoga. Then alone you stop doing Karma Yoga. Karma and Janana cannot be practiced at the same time. Karma - you have to believe that you are doer. To know Brahman, self has to go away. They are mutually exclusive practices - Karma and Jnana.
3. Bhedabheda - `Radically different` - And Vishitha Advaitya - Karma and Jnana Samuchya - Bhaskara - no bhakti - `upasana`. Achintya - Bhakti is the highest. Karma and Jnana are not required. That is optional.
Vishistha Advaita - Ramanuja - most aligned to Ramakrishna.
4. Ramanuja - Jnana Yoga - you meditate on your true nature - soul, beyond body-mind complex. Similar to Ramakrishna.
![](Vishistha-Advaita.png)
5. Is god's grace is needed? All Bhakti schools need it. Bhaskara's Aupadhika - god's grace plays  no role. Sankara is devotional - God is the tiger in the dream who helped us wake up.
6. Sat Chit Ananda are not attributes as per Advait. These are related in a negative way pointing to the nirguna. Dvat - face value.
7. Shuddha advaitha - Krishna is the soul reality. One reality is Krishna. One tiny aspect of Krishna is nirguna. Krishna manifest as world.
######IV. Q & A
1. Is Advait Vedanta a culmination of all the dualistic philosophies? Going by a mundane experience, when we are in love, deep love with another human being, we tend to feel her always close to ourselves - in our thoughts, in our meditations, and there is a time, we see her in people around us. In the ultimate phase of this deep, pure love, one can see parts of her in one's own self. And then, a time comes, when the love deepens further, and the love is felt in the chirping of birds, meandering of streams, and blossoming of flowers. What started with a gross attraction for another body and mind, ends to be universal love that transcends personality and personal boundaries. Is not that Advait? Is not Advait the culumination of all theistic and non theistic approaches to relate to Brahman? Or for that matter relating to anything and anybody in that true devotion? Is not Advait only about Bhakti? Is not it the ideal towards which we all should strive, if we are intersted to know what pure love can do to us!?
2. Saguna Nirakara - is it true? Sankara - Nirguna and Nirakara. Ramanuja - Vishnu is personal and sakaar. `Archavatara` - The physical form of a God - archavatara - God is embodied into that idol. That is avtaara.
3. Avatara -
   - Advaita - two tier realities. It is true from emperical level. They give a provisional reality. Ramkrishna is similar to Ramanuja
   - No advaitya - Bhaskara
   - Achintya - embraces Avatara - center piece.
4. Is maya related to Shakti?
In advaita vedanta - no mention of Shakti. maya plays role. Maya is neither real or unreal.
5. Is Shakti of Ramakrishna and Achintya Vedabheda different?
Ramkrishna says - When I think of God as inactive - Brahman. When I think of the same God who creates, preserves or destroys the universe - Kali. Achintya Bhedabheda - Brahman and Shakti are inseparable. The key difference is that nirguna Brahman is lower than Saguna.
6. Mahdahava - World creation is being overseen by God. God is difference from the world or its seed.
7. Let salvation take different forms. Based on what the Bhakta loves.

######V. Reference books
For further explanation and reading on this subject you can refer to the following:
1. Swami Tapasyananda, Bhakti Schools of Vedānta: Lives and Philosophies of Rāmānuja, Nimbārka, Madhva, Vallabha, and Caitanya (Madras: Sri Ramakrishna Math, 1990).

2. Satischandra Chatterjee and Dhirendramohan Datta, “The Monism of Śaṅkara (Advaita),” in Chatterjee and Datta, An Introduction to Indian Philosophy (Calcutta: University of Calcutta Press, 1939), pp. 365-413.
