<span style="color:; font-family:Menlo, Consolas, DejaVu Sans Mono, monospace; font-size: 12pt ;">

####ISA UPANISHAD : The uncompromising reconciliation of  uncompromising extremes!

<INS>THE OPPOSITES</INS> - The pairs of opposites successively taken up by the Upanishad and resolved are, in the order of their succession:
1. The Conscious Lord and phenomenal Nature.
2. Renunciation and Enjoyment.
3. Action in Nature and Freedom in the Soul.
4. The One stable Brahman and the multiple Movement.
5. Being and Becoming.
6. The Active Lord and the indifferent Akshara Brahman.
7. Vidya and Avidya.
8. Birth and Non-Birth.
9. Works and Knowledge.
These discords are thus successively resolved:

---
######I. God and the World
>ॐ ईशा वास्यमिदँ सर्वं यत्किञ्च जगत्यां जगत् ।

All this is for habitation by the Lord, whatsoever is individual universe of movement in the universal motion.
The first line affirms the panentheistic view that God is both immanent and transcendent: He not only transcends the world but also rules and inhabits it. Applying the Etymological Principle, Sri Aurobindo brings out the philosophical significance of the phrase “jagatyāṃ jagat” by emphasizing the derivation of both words from √gam (“to go”), which indicates motion. For Sri Aurobindo, the unusual pairing of the feminine noun “jagatī” in its locative singular form with the masculine noun “jagat” in its nominative singular form expresses the dialectical relationship between the microcosm and the macrocosm. The etymological kinship of the two words suggests that the “individual universe of movement” (jagat) is one with the “universal motion”  (jagatī): every object in the universe is nothing but the frontal appearance of the entire universe. At the same time, the microcosm is contained in the macrocosm, since the individual “partakes of the nature of the universal” (IU 16). The second line of the first verse, Sri Aurobindo argues, resolves the opposition between renunciation and enjoyment by telling us to enjoy the world through renunciation: tena tyaktena bhuñjīthāḥ. For Sri Aurobindo, the Īśā Upaniṣad redefines the concepts of enjoyment (bhoga) and renunciation (tyāga) so as to show their mutual compatibility

Phenomenal Nature is a movement of the conscious Lord. The object of the movement is to create forms of His consciousness
in motion in which He as the one Soul in many bodies can take up his habitation and enjoy the multiplicity and the movement
with all their relations.

######II. Renunciation & Enjoyment
>तेन त्यक्तेन भुञ्जीथा मा गृधः कस्यस्विद्धनम् ।। १ ।।

By that renounced thou shouldst enjoy; lust not after any man’s possession. Real integral enjoyment of all this movement and multiplicity in its truth and in its infinity depends upon an absolute renunciation; but the renunciation intended is an absolute renunciation of the principle of desire founded on the principle of egoism and not a renunciation of world-existence. 2 This solution depends on the idea that desire is only an egoistic and vital deformation of the divine Ananda or delight of being from which the world is born; by extirpation of ego and desire Ananda again becomes the conscious principle of existence. This substitution is the essence of the change from life in death to life in immortality. The enjoyment of the infinite delight of existence free from ego, founded on oneness of all in the Lord, is what is meant by the
enjoyment of Immortality.

######III. External action and inernal action
>कुर्वन्नेवेह कर्माणि जिजीविषेच्छत समाः ।
एवं त्वयि नान्यथेतोऽस्ति न कर्म लिप्यते नरे ॥ २॥

Doing verily works in this world one should wish to live a hundred years. Thus it is in thee and not otherwise than this; action cleaves not to a man.

असुर्या नाम ते लोका अन्धेन तमसाऽऽवृताः ।
तामस्ते प्रेत्याभिगच्छन्ति ये के चात्महनो जनाः ॥ ३॥

######IV. One stable Brahman and multiple movements
>अनेजदेकं मनसो जवीयो नैनद्देवा आप्नुवन्पूर्वमर्षत् ।
तद्धावतोऽन्यानत्येति तिष्ठत्तस्मिन्नपो मातरिश्वा दधाति ॥ ४॥
तदेजति तन्नैजति तद्दूरे तद्वन्तिके ।
तदन्तरस्य सर्वस्य तदु सर्वस्यास्य बाह्यतः ॥ ५॥


######V. Being and Becoming
>यस्तु सर्वाणि भूतान्यात्मन्येवानुपश्यति ।
सर्वभूतेषु चात्मानं ततो न विजुगुप्सते ॥ ६॥
यस्मिन्सर्वाणि भूतान्यात्मैवाभूद्विजानतः ।
तत्र को मोहः कः शोक एकत्वमनुपश्यतः ॥ ७॥

######VI. The active Lord and the indifferent Akṣara Brahman
>स पर्यगाच्छुक्रमकायमव्रण मस्नाविरँ शुद्धमपापविद्धम् ।
कविर्मनीषी परिभूः स्वयम्भू र्याथातथ्यतोऽर्थान् व्यदधाच्छाश्वतीभ्यः समाभ्यः ॥ ८॥

######VII. Vidyā and Avidyā
>अन्धं तमः प्रविशन्ति येऽविद्यामुपासते ।
ततो भूय इव ते तमो य उ विद्यायाँ रताः ॥ ९॥
अन्यदेवाहुर्विद्ययाऽन्यदाहुरविद्यया ।
इति शुश्रुम धीराणां ये नस्तद्विचचक्षिरे ॥ १०॥
विद्यां चाविद्यां च यस्तद् वेदोभयं सह ।
अविद्यया मृत्युं तीर्त्वा विद्ययाऽमृतमश्नुते ॥ ११॥

You cannot ignore Personal God. You can end up in the state of void. Or state of selfishness. Vignani sees that impersonal Brahman in everything in the world. That helps him to be more productively engaged with the world.

######VIII. Birth and non-birth
>अन्धं तमः प्रविशन्ति येऽसम्भूतिमुपासते ।
ततो भूय इव ते तमो य उ सम्भूत्याँ रताः ॥ १२॥
अन्यदेवाहुः सम्भवादन्यदाहुरसम्भवात् ।
इति शुश्रुम धीराणां ये नस्तद्विचचक्षिरे ॥ १३॥
सम्भूतिं च विनाशं च यस्तद् वेदोभयँ सह ।
विनाशेन मृत्युं तीर्त्वा सम्भूत्याऽमृतमश्नुते ॥ १४॥

######IX. Works & Knowledge
>हिरण्मयेन पात्रेण सत्यस्यापिहितं मुखम् ।
तत्त्वं पूषन्नपावृणु सत्यधर्माय दृष्टये ॥ १५॥
पूषन्नेकर्षे यम सूर्य प्राजापत्य व्यूह रश्मीन् समूह तेजः ।
यत्ते रूपं कल्याणतमं तत्ते पश्यामि योऽसावसौ पुरुषः सोऽहमस्मि ॥ १६॥
वायुरनिलममृतमथेदं भस्मांतꣳ शरीरम् ।
ॐ क्रतो स्मर कृतꣳ स्मर क्रतो स्मर कृतꣳ स्मर ॥ १७॥
अग्ने नय सुपथा राये अस्मान् विश्वानि देव वयुनानि विद्वान् ।
युयोध्यस्मज्जुहुराणमेनो भूयिष्ठां ते नमौक्तिं विधेम ॥ १८॥
