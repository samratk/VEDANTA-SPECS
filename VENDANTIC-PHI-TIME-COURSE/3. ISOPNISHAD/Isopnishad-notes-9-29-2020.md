<span style="color:; font-family:Menlo, Consolas, DejaVu Sans Mono, monospace; font-size: 12pt ;">

######Reflective Questions
1. According to Sri Aurobindo, what is the unifying principle of the Īśā Upaniṣad?

The reconciliation of apparent oppsites.

2. How does Śaṅkara interpret the word “bhuñjīthāḥ” in the first verse of the Īśā Upaniṣad?

Sankara - protect. Protect Atman. Implied.

3. How do Swami Vivekananda and Sri Aurobindo interpret the word “bhuñjīthāḥ” in the first verse of the Īśā Upaniṣad?

Both interpret Bhunjita as enjoy. Enjoy the world having first `Renunciated all desires`. Looking at the world as real manifestation of God.

4. How does Śaṅkara explain the connection between the first and second verses of the Īśā Upaniṣad?

Shankara claims the first verse is addressed to uttam adhikary. The second verse is addressed to inferior aspirants aspirants who are eligible to practice karma yoga.

5. How does Śaṅkara’s interpretation of the second verse of the Īśā Upaniṣad differ from the interpretations of Swami Vivekananda and Sri Aurobindo?

Neither Vivekananda or Aurobindo, say second is lower than higher. Both take the entire upanishad as a whole. They both claim that the same person who see God in everything and renunciate the desires should work 100s.

6. What are some of Sri Aurobindo’s main criticisms of Śaṅkara’s interpretation of the Īśā Upaniṣad?

One of his criticism is that with regard to 1st verse, Shankar adds, unrithm, before sarvaidam.
For second verse as a concession to the ignorant, is not justified.

7. How might Sri Ramakrishna’s teachings on vijñāna have informed Sri Aurobindo’s interpretation of verses 1, 2, and eight of the Īśā Upaniṣad?

Verse 1 - Vijnani - God is in everything in Universe. And as everything.
2nd line of first verse - tena taktena bhunjita - Vijnani sees the world as Mojar Kuti. Mansion of Mirth.

Verse 2- Vijnani cares of the world. Jnani is selfish. Jeevanmukta should continue to work in the world to help those laboring with their ignorance. They are always trying to help those who are ignorant.

Brahman is both personal and impersonal. Vijnani realizes that divine reality is nirguna as well as saguna. Immanent and transcending.

8. Which interpretation of the Īśā Upaniṣad do you think is more faithful to the scripture—Śaṅkara’s or Sri Aurobindo’s? Why?



---

######Class Notes
1. vijñāna and Jnana - Personal and Imporsonal - Transcendence and Immanance.
