<span style="color:; font-family:Menlo, Consolas, DejaVu Sans Mono, monospace; font-size: 12pt ;">

###### Class Notes
`Views` -
1. Sectarian approach - he is multi dimensional. You cannot reduce. He is an avatara.
   - Vishishtha Advaith
   - Tantric.
2. No philosophy - no education - Its opposite. With one teaching you can write books.
3. No sectarian approach
4. Samanvayi, Samanvayi Vedanta, Neo Vedanta, Vignana Vedanta.

`Interpretative principles` -
1. Understand on their own terms - terms of the teaching itself.
2. The context provide crucial poiners - context of diaglogues
   - With whom he is talking to.
   - Is it in response to what someone is asking.
   - Verbal cues - Page 691 - In the life of vedantic reasoning, everything is illusion. For me i consider something.
   - His vowed non sectarianism needs to be taken into account.
   - Accepts the spiritual core of all spiritual sect without subscribing into their doctrine which does not make sense.
     - Nirvikalpa Samadhi - Advait
     - Vatsalya, Madhuriya - Vashnava
     - Tantric
     - Ramanuja
   - Interpret based on his own teachings. Not something else.

`Doctrines`
1. After nirvikalpa samadhi, there are people - ishvara koti - incarnation of God or part of one of these incarnations - who retrun to the world and live life as a vijnani. Roof - Brahma Jnana. After that he sees the steps are made of the same material. So reality is nirguna as well as saguna. Mojar Kuti. Vijnani are here to help others. Eg - Narada.
2. Teachings of Vijnana can be found in Vedanta. Read the Gita, Bhagavad Purana, and the Upnishads. you will get it. Adhyatma Ramayana.

`Vijnana Vedanta`
1. Jnana and Vijnana in Gita was interpreted by Sri Aurobindo as integral knowledge.
2. Reasoning -
   - sat and asat. Viveka.
   - Learned ignorance - Through reasoning you realize that reason has its own limitation. The tiredness of the marathon runner. Kathopnishad - 1.2.23.
   - Infinite divine reality - real nature - both personal and impeorsonal. Both immanent and transcendent. You cannot put any limit to God. Nirguna Brahhman is also Bhagavan. Brahman and Shakti are Aveda.
   - Brahman alone is reality. All other is false. But Vijnani sees the world as manifestation of Shakti and not unreal. He sees there is nothing but God. And that is real. Shankara is world negating. Brahman Satya. Jagat Real. All is real. Life is based on Truth. This world is a real manifestation of God. This aligns with Life Divine of Sri Aurobindo. Eg - can be found in all Upnaishad. 3.14.1 - Chandogya Upanisad - Sarvam Khaluvidam Brahman. - All this is Brahman. The names and forms are manifestation of Brahman. Everything is like wax.
   - Nithya and Lila - Different planes of consciousness - all of them are true. Vatsalya, Shanta, Madhura (lover),  - different ways to relate to God. It makes it more richer. Loving relationships with God.
   - Various religious faith are equally effective path to reach God. God is infinite. Paths to God are infinite.
   -
###### 1. Reflective Questions
1. What are the three main ways that scholars have interpreted Sri Ramakrishna’s philosophy?

 2. How did Sri Ramakrishna’s various spiritual practices shape his philosophical teachings?

 3. Why is it important to understand the context of Sri Ramakrishna’s teachings as recorded in the Gospel? What kinds of context are important?

 4. According to Sri Ramakrishna, what is the difference between jñāna and vijñāna?

 5. Why does Swami Medhananda call Sri Ramakrishna’s philosophy “Vijñāna Vedānta”?

 6. What does Sri Ramakrishna mean when he says that “Brahman and Śakti are inseparable [abheda]”? What is significant about the various analogies he uses to illustrate this teaching (such as fire and its power to burn, and so on)?

 7. Would Śaṅkara accept Sri Ramakrishna’s teaching that Brahman and Śakti are inseparable? Why or why not?

 8. According to Sri Ramakrishna, is the world real or unreal?

 9. Which passages from the Upaniṣads and the Bhagavad-Gītā support Sri Ramakrishna’s philosophical teachings?

 10. How are Sri Ramakrishna’s teachings about vijñāna relevant to his teachings on the harmony of all religions?
