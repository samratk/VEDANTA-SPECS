<h1>Scheme of Patanjali’s Yogasutras</h1>

<h2> Showing the connection between far flung sutras and different names for the same things </h2>


1. **`The Goal`** – Seer staying in his own nature (1, 3) – through restraining `total waves of the mind (1,2) = Asamprajnata = Nirbija`

2. **`Wrong Path`** – Direct attempt at restraining the waves (1, 18) – leads to position of becoming gods or merging in Prakriti, still transmigration experienced (1,19).

3. **`Right Path`** described in 1st chapter –
   1. Develop self-conscious (`Samprajnat`) concentration
   2. Attain (`Samadhi`) (`1, 17`)
   3. Gain inner truthful knowledge (`Ritambhara Prajna`) (`1, 20; 1, 48`) and then with that power and knowledge
   4. Go for `restraining the waves (1, 20)`.
   5. The `Sanskaras` of that full concentration and knowledge will `restrain all other Sanskras (1, 50)` and then
   6. Restraining them will restrain total Sanskaras which will give the `seedless Concentration (1, 51)`.

4. This `Samprajnata Concentration` happens in some grades `(1, 17)` as
   1. `Vitarka` on `gross objects` subdivided into
      1. `savitarka (1, 42)` and
      2. `nirvitarka (1, 43)`,
   2. `Vichara` on subtle objects subdivided into
      1. `savichara` and
      2. `nirvichara` `(1, 44)`
          1. `Ananda` on one’s own mind and buddhi,
          2. `Asmita` on ‘I’ consciousness. 
      Ananda and Asmita grades become included in extended definition of savichara and nirvichara (1, 45). All these are Sbija or with seeds of transmigration (1, 46), but they will restrain all other sanskaras and also bring inner knowledge filled with truth.
If it is not possible to concentrate due to vagaries of mind then various alternatives and supplementary practice are prescribed in Chapter 1 sutras 21 to 39. Or preliminary steps are given in 2nd chapter called Sadhanapad the chapter on means. Practice Tapah = austerities, Swadhyaya = repetition of mantra and Ishwarapranidhana = dedication of fruits of work to God (2, 1) and then try to practice concentration. RATHER practice 8 steps for achieving full concentration and inner knowledge by removing impurities which are obstructing concentration (2, 28-29). 1. Yama = general rules (2, 30). 2. Niyama = Special rules including those given in 2,1 (2, 32). 3. Asana = posture (2, 46). 4. Pranayama = regulation of breathing (2, 49). 5. Pratyahara = Organs instead of going to objects remain with the mind (2, 54). And then –
We come to the 3rd chapter for the three steps which are parts of the process of concentration. 6. Dharana =  holding the mind to a particular point (3, 1) this is equvivalent of vitarka (1, 42-43). Dhyana = An unbroken flow of knowledge through that point (3, 2) is the process from vitarka upto asmita (1, 17, 44-45). Samadhi = Full concentration revealing the innermost meaning of the point (3, 3) this is equvivalent of the ultimate samprajnata (1, 17, 47). From there to the Final Goal Asamprajnata by restraining all waves.


Observations from Vedanta
In 1, 13 Patanjali says “then” and in 1, 4 “otherwise some form as per wave”. Hence it is not permanent. He hopes to make it permanent 1, 14 by “deerghakala, nairantarya and satkarasevito” but something which is temporary by its very nature can never be made permanent by ‘practice’. That is why Acharya Gadapada says – “If slightest difference is admitted between things (between various Purushas and between Purusha and Prakrit) then permanent unattachment itself is not possible,what to speak of dropping of the veil to reveal the nature of Atman.”
