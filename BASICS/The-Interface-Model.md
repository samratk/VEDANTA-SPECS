<h5>The Interface Model </h5>

The base class template - the interface - it is the Brahman.

There are many classes inherited from that base class. We human beings in our flesh, emotions, intellect, Vasanas, judgements, desire, anger, attachments, greed, arrogance,jealosy,anxieties, fears and confusions are the objects which are the instantiations of the most derived class.

Not only that there is a factory pattern which instantiates multifarious instances of that most derived class after various further levels of derivations, as we continue to live life, accumulating more and more impressions from the world. 

Now when we live this life from the vantage point of the object we are, we obviously have lost in touch with all the methods and attributes of our base class. All the methods and attributes are either overwritten or overloaded. We have lost that awareness that we are actually in the basics and pure substratum that base class. 

We also treat the people based on the most derived objects that they have made of themselves. 

The path of Advaith Vedanta is to relate to self and all from that common base interface from which we all are derived from. Not only that it reminds us of the attributes and the methods of that base class. 

It goes a step further and directs us to relate to people from those pure base class interfaces that are exposed. And at the same time reminds us that when we see impure Behaviors from others we need to ignore them as Maya. They are termed as Mithya as they are not part of the original pure base class interface. 

Hence it is imperative to treat all as Gods. And hence it is said all is Brahman! 

The exact thing that was taking away my peace was the feeling that she was using using me. That she had this sadistic inclination to have someone cry for her. That she always wanted me as a backup. I was able to apply this situation to this interface model.

All these negativeness even if is they are real, are only the leaf nodes of the class diagram. They are not real. Because I need to relate to all human beings from attitude of dealing with them from the base interface class and the pure virtual method and attirbutes. That is Brahman. For her and for any human those are Godly.

And moreover, I need to practice `Uparati`. The goal is not to understand and go into how these polymorphisms are implmented and instantiated. That is the work of the psychologists. But for a Vedantin as me, the search starts and end with God realization. So, I need to just focus on the base interface. Thats it.  
