1. Kaam - Desire - intense passion or lust
2. Krodha - Anger
3. Lobha - Greed
4. Madha - Arrogance
5. Moha - Delusion, Attachment
6. Matsarya - Jealousy, competition

By following Vedanta, the above will `reduce`. They might not disappear. 
