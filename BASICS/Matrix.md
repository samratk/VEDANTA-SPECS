
Problem  |  Solution |  Method
--|---|--
`Chitta mala` - impurtity of mind  | `Chitta suddhi`  |  `Karma Yoga`   |   |
`Vikshepa` (Lack of concentration. Restless mind. Lack of attention)  | Focus. Concentration `Ekagrata`| `Upasana`. Meditation. Forebearance. A big shot is a small shot who keeps shooting.
Ingnorance - `Avidya`  | Knowledge. `Brahma Gnana`. Knowledge of Brahman  | `Gnana Yoga`
