`Sat` - This is not an experience of existence. Rather all existence is based on the top of this.
`Chit` - This is not an experience of awareness. Rather it is that which illumines all other awareness.
`Ananda` - This is not an experience of happiness. This is rather joy itself, on which all other experiences of joy are based out of. 
