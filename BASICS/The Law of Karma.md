1. The law of Gravity you cannot break. The law of Karma too you cannot break. There are three results of each karma -
   1. Karma Phala - The immediate physical results.
   2. Psychological result - You develop a psychological habit of sharing and caring. It leads to wellbeing.
   3. Karma Adhyaksha - God holds the good deeds
2. The play of Karma -

```mermaid
graph LR;
  A([Right Action])-->B([Punya - Merit])-->C([Happiness Sukha])
  D([Wrong Action])-->E([Paap - Demerit])-->F([Sorrow - Dukha])
```
