1. In the state of our deep sleep, we have no mind working. But still we have an awareness that we had a deep sleep. That points to Brahman within - The real self.
2. When we are in a dark room and eyes are shut too, we do not have any awareness of anything. Even if we do not touch ourselves or hear ourselves, with our breath stopped, still we have an awareness of the self. That is the a pointer of Brahman - the real self. It is still a pointer as that awareness is conceived by a thought patter in the mind.
3. 
