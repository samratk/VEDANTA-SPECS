<span style="color:; font-family:Menlo, Consolas, DejaVu Sans Mono, monospace; font-size: 12pt ;">

किम करस्य
किम करोति
क्रितो अहम्

BY the servant of the servant I have been made the servant.

1. A man feels incomplete. So he marries. A woman feels incomplete so she marries. We feel incomplete so we earn money. We are incomplete so we build houses. We are incomplete so we struggle. Some people because they feel incomplete go of accumulating knowledge.
2. By that marriage nobody becomes complete. Then what is the way?
3. By nature every person is full and complete. So he struggles.
4. A man has perfection within. To manifest that perfection the man needs to make changes within himself. Man is an animal who is complete, and feels incomplete, and knows that he needs to change to make himself complete - `Sadhaka`
5. The different pramana - means of proving -
A) Direct Perception
 - 1. Pratyaksha pramana -
    - Subjective Perception - The knowledge we get from our 5 senses. Example - I am happy or i am miserable. Can our indriyas show that? Do I not know I am  happy without any sense organs? This is subjective awareness. It is direct. Feeling of love for someone. I am directly aware that I am loving someone. I hate someone.
    - Waves goes to mind, and then through my sense organs, it goes to an object outside through my sense organs. Brahman -> Mind -> I consciousness & one part of that goes out to the object.
B) Indirect means of knowledge
- 2. Anupamaa
- 3. Upamana
- 4. Inductive logic - assumption of something to prove something - Arthapatti - person is fat. must be eating at night as he does not eat in day.
- 5. Anupalabdhi pramana - not available is the pramana - non availability. Example - absence of pen on the table.
- 6. Shabda Pramana - Aapta vakya - Verbal testimony.
  - 6a. Guru parampara.
  - 6b. Vedic praman - Rishi vakya.

6. Out of the 6 pramanas, Pratyaksha pramana impacts our life the most. any theory that world is unreal does not change our lives at all. It does not touch our lives.
7.
